const termLength = 30
let graphIndex = 100
let literalIndex = 0
let blankIndex = 0
const differentNodesForLiteral = true
let tripleSets = []
let graph = {nodes:[], links:[]}

function triplesFromURL (url) {
  return fetch('/jsonld?url=' + url)
    .then(res => res.json())
}

function getGraphId (i) {
  return new String((121 + 31 * i) % graphIndex)
}

function filterTypeTriples(triples) {
  let filtered = []
  for (let triple of triples) {
    if (triple.predicate.value !== 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type') {
      filtered.push(triple)
    }
  }
  return filtered
}

function escapeBlankNodeOfTriples (triples, graphId) {
  const bnMap = {}

  function escapeTerm (term, termType) {
    if (termType === 'bnode') {
      const key = graphId + '_' + term
      if (bnMap[key] === undefined) {
        bnMap[key] = blankIndex++
      }
      return 'b' + bnMap[key]
    }
    return term
  }

  triples.forEach(triple => {
    triple.subject.value = escapeTerm(triple.subject.value, triple.subject.type)
    triple.object.value = escapeTerm(triple.object.value, triple.object.type)
  })

  return triples
}

function filterNodesById (nodes, id) {
  return nodes.filter(function (n) { return n.id === id })
}

function termToString (term) {
  const replacementMap = {
    'http://stardog.com/tutorial/': 'ex:',
    'http://schema.org/': 'sch:',
    'http://ogp.me/ns#': 'og:',
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#': 'rdf:',
    'http://www.w3.org/2000/01/rdf-schema#': 'rdfs:',
    'http://www.w3.org/1999/xhtml/vocab#': 'vocab:',
    'http://ogp.me/ns/fb#': 'og:fb:',
    'http://purl.org/dc/elements/1.1/': 'purl:',
    'http://purl.org/dc/terms/': 'purl:',
    'file:///home/labo/': 'local:',
    'http://www.edutella.org/bibtex#': 'bib:',
    'http://www.w3.org/2001/vcard-rdf/3.0#': 'vcard:',
    'https://schema.org/': 'sch:'
  }
  let value = term

  for (const d in replacementMap) {
    console.log(value)
    value = value.replace(d, replacementMap[d])
  }

  return value
}

function termToNodeId (term, termType) {
  if (differentNodesForLiteral &&
      termType === 'literal') {
    return 'lit' + literalIndex++
  }

  return termToString(term)
}

function insertTriplesToGraph (triples, graphId) {
  // Initial Graph from triples
  triples.forEach(triple => {
    const subjId = termToNodeId(triple.subject.value, triple.subject.type)
    const predId = termToNodeId(triple.predicate.value, triple.predicate.type)
    const objId = termToNodeId(triple.object.value, triple.object.type)

    let subjNode = filterNodesById(graph.nodes, subjId)[0]
    let objNode = filterNodesById(graph.nodes, objId)[0]

    if (subjNode == null) {
      subjNode = { id: subjId, label: subjId, graph: graphId, weight: 1 }
      graph.nodes.push(subjNode)
    }

    if (objNode == null) {
      objNode = { id: objId, label: termToString(triple.object.value), graph: graphId, weight: 1 }
      graph.nodes.push(objNode)
    }

    graph.links.push({ source: subjNode, target: objNode, predicate: predId, graph: graphId, weight: 1 })
  })
}

function update() {
  graph = { nodes: [], links: [] }

  for (let i = 0; i < tripleSets.length; i++) {
    insertTriplesToGraph(tripleSets[i], getGraphId(i))
  }
  updateViz()
}

const chartWidth = 0.9 * getWidth()
const chartHeight = 0.75 * getHeight()

const svg = d3.select("#svg-body").append("svg")
      .attr("width", chartWidth)
      .attr("height", chartHeight)
      .call(d3.zoom().on("zoom", function () {
        svg.attr("transform", d3.event.transform)
      }))
      .append('g')

function updateViz(){
  // turbo https://github.com/d3/d3-scale-chromatic/blob/master/src/sequential-multi/turbo.js
  const color =  function(t) {
    t = Math.max(0, Math.min(1, t));
    return "rgb("
      + Math.max(0, Math.min(255, Math.round(34.61 + t * (1172.33 - t * (10793.56 - t * (33300.12 - t * (38394.49 - t * 14825.05))))))) + ", "
      + Math.max(0, Math.min(255, Math.round(23.31 + t * (557.33 + t * (1225.33 - t * (3574.96 - t * (1073.77 + t * 707.56))))))) + ", "
      + Math.max(0, Math.min(255, Math.round(27.2 + t * (3211.1 - t * (15327.97 - t * (27814 - t * (22569.18 - t * 6838.66)))))))
      + ")";
  }
  
  svg.html("");

  // ==================== Add Marker ====================
  svg.append("svg:defs")
    .append("svg:marker")
    .attr("id", "end")
    .attr("refX", 14)
    .attr("refY", 3)
    .attr("markerUnits", 'userSpaceOnUse')
    .attr("markerWidth", 8)
    .attr("markerHeight", 10)
    .attr("orient", 'auto')
    .append('path')
    .style("fill", "#dddddd")
    .attr("d", 'M 0 0 6 3 0 6 1.5 3');
  
  // ==================== Add Links ====================
  var links = svg.selectAll(".link")
      .data(graph.links)
      .enter()
      .append("line")
      .attr("marker-end", "url(#end)")
      .attr("class", "link")
      .attr("stroke-width",1)
  ;//links

  // ==================== Add Link Names =====================
  // http://bl.ocks.org/fancellu/2c782394602a93921faff74e594d1bb1
  const edgepaths = svg.selectAll(".edgepath")
        .data(graph.links)
        .enter()
        .append('path')
        .attr('class', 'edgepath')
        .attr('fill-opacity', 0)
        .attr('stroke-opacity', 0)
        .attr('id', function (d, i) {return 'edgepath' + i})
        .style("pointer-events", "none")

  const edgelabels = svg.selectAll(".link-text")
        .data(graph.links)
        .enter()
        .append('text')
        .style('pointer-events', 'none')
        .attr('class', 'link-text')
        .attr('id', function (d, i) {return 'edgelabel' + i})
  // .attr('filter', 'url(#text-shadow)' );;
  edgelabels.append('textPath')
    .attr('xlink:href', function (d, i) {return '#edgepath' + i})
    .style("text-anchor", "middle")
    .style("pointer-events", "none")
    .attr("startOffset", "50%")
    .text(function (d) {return d.predicate})
  //linkTexts.append("title")
  //		.text(function(d) { return d.predicate; });

  // ==================== Add Node Names =====================
  var nodeTexts = svg.selectAll(".node-text")
      .data(graph.nodes)
      .enter()
      .append("text")
      .attr("class", "node-text")
      .text( function (d) {
        const label = d.label
        if (label.length > termLength) {
          return label.substring(0, termLength) + '...'
        } else {
          return label
        }
      })

  nodeTexts.append("title")
    .text(function(d) { return d.label; });

  // ==================== Add Node =====================
  var nodes = svg.selectAll(".node")
      .data(graph.nodes)
      .enter()
      .append("circle")
      .attr("class", "node")
      .attr("r", 8)
      .style('fill', d => color(parseInt(d.graph) / graphIndex))
      .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended))
  
  function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }
  //nodes

  // ==================== Force ====================
  function ticked() {
    nodes
      .attr("cx", function(d){ return d.x; })
      .attr("cy", function(d){ return d.y; })
    ;
    
    links
      .attr("x1", 	function(d)	{ return d.source.x; })
      .attr("y1", 	function(d) { return d.source.y; })
      .attr("x2", 	function(d) { return d.target.x; })
      .attr("y2", 	function(d) { return d.target.y; })
    ;
    
    nodeTexts
      .attr("x", function(d) { return d.x + 12 ; })
      .attr("y", function(d) { return d.y + 3; })
    ;

    edgepaths.attr('d', function (d) {
      return 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y;
    })
    
    edgelabels.attr('transform', function (d) {
      if (d.target.x < d.source.x) {

        const rx = d.source.x + (d.target.x - d.source.x) /2
        const ry = d.source.y + (d.target.y - d.source.y) /2
        return 'rotate(180 ' + rx + ' ' + ry + ')';
      }
      else {
        return 'rotate(0)';
      }
    })

  }

  // ==================== Run ====================

  var simulation = d3.forceSimulation()
      .force("charge", d3.forceManyBody().strength(-50))
      .force("link", d3.forceLink(graph.links).id(function(d) { return d.id }).distance(100))
      .force("center", d3.forceCenter(chartWidth / 2, chartHeight / 2))

  simulation
    .nodes(graph.nodes)
    .on("tick", ticked)

  simulation.force("link")
    .links(graph.links)
}


// utils

function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}

function getHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
}
